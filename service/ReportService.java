/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.util.List;

/**
 *
 * @author Melon
 */
public class ReportService {
    public  List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
         return dao.getDayRerport();
    }
    
    public  List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
         return dao.getMonthReport(year);
    }
    
    public List<ReportArtist> getReportArtistByMonth(int year) {
        ArtistDao dao = new ArtistDao();
        return dao.getReportArtistByMonth(year);
    }

}
