/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.util.List;
import com.waratchaya.albumproject.model.ReportArtist;
import com.waratchaya.albumproject.model.ReportSale;
import com.waratchaya.albumproject.service.ReportService;

/**
 *
 * @author Melon
 */
public class TestepotArtist {
     public static void main(String[] args) {
        ReportService reportService = new ReportService();
        List<ReportArtist> report = reportService.getReportArtistByMonth(2013);
        for (ReportArtist r : report) {
            System.out.println(r);
        }
    }
}
