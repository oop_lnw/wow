/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Melon
 */
public class ReportArtist {

    String name;
    double total;
    String period;

    public ReportArtist(String name, double total, String period) {
        this.name = name;
        this.total = total;
        this.period = period;
    }

    public ReportArtist() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "ReportArtist{" + "name=" + name + ", total=" + total + ", period=" + period + '}';
    }

    public static ReportArtist fromRS(ResultSet rs) {
        ReportArtist obj = new ReportArtist();
        try {
            obj.setName(rs.getString("name"));
            obj.setPeriod(rs.getString("period"));
            obj.setTotal(rs.getDouble("total"));
        } catch (SQLException ex) {
            Logger.getLogger(ReportArtist.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

}
